package main

import (
	"io"
	"net/http"
	"os"
	"sample/gin-poc/controller"
	"sample/gin-poc/middleware"
	"sample/gin-poc/service"

	"github.com/gin-gonic/gin"
	//gindump "github.com/tpkeeper/gin-dump"
)

var (
	videoService service.VideoService = service.New()
	loginService service.LoginService = service.NewLoginService()
	jwtService   service.JWTService   = service.NewJWTService()

	videoController controller.VideoController = controller.New(videoService)
	loginController controller.LoginController = controller.NewLoginController(loginService, jwtService)
)

func setupLogOutput() {
	f, _ := os.Create("api.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main() {
	setupLogOutput()

	server := gin.New()

	server.Static("/css", "./template/css")
	server.LoadHTMLGlob("template/*.html")

	// Add gindump.Dump() to remove debugging log
	// Add middleware.Logger() for logging in file
	// Add middleware.BasicAuth() for user/pwd auth
	server.Use(gin.Recovery(), gin.Logger())

	// Login Endpoint: Authentication + Token creation
	server.POST("/login", func(ctx *gin.Context) {
		token := loginController.Login(ctx)
		if token != "" {
			ctx.JSON(http.StatusOK, gin.H{
				"token": token,
			})
		} else {
			ctx.JSON(http.StatusUnauthorized, nil)
		}
	})

	apiRoutes := server.Group("/api", middleware.AuthorizeJWT())
	{
		apiRoutes.GET("/videos", func(ctx *gin.Context) {
			ctx.JSON(200, videoController.FindAll())
		})

		apiRoutes.POST("/videos", func(ctx *gin.Context) {
			err := videoController.Save(ctx)
			if err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{
					"error": err.Error(),
				})
			} else {
				ctx.JSON(http.StatusOK, gin.H{
					"status": "Video input is valid",
				})
			}

		})
	}

	viewRoutes := server.Group("/view")
	{
		viewRoutes.GET("/videos", videoController.ShowAll)
	}

	if os.Getenv("PORT") != "" {
		server.Run(":" + os.Getenv("PORT"))
	} else {
		server.Run()
	}
}
