# Golang / Go Gin Framework Crash Course

https://youtube.com/playlist?list=PL3eAkoh7fypr8zrkiygiY1e9osoqjoV9w


https://gitlab.com/pragmaticreviews/golang-gin-poc

## 01 - Getting started
- `GET` List videoes in list
- `POST` Add new video

## 02 - Middleware
- Logging format
- Basic Authorization
- Debug [gindump]

## 03 - Data Binding & Validation
### Sample Request
#### `POST` /videos
Request Body
```json
{
    "title": "Video A",
    "description": "goood desc",
    "url": "https://youtube.co.th",
    "author": {
        "firstname": "Adam",
        "lastname": "Smith",
        "age": 20,
        "email": "eee@fuck.up"
    }
}
```

## 04 - HTML, Templates and Multi-Route Grouping
### HTML Template
Similar to django

### Route Grouping
```golang
apiRoutes := server.Group("/api")
{
    apiRoutes.GET("/videos", func(ctx *gin.Context) {
        ctx.JSON(200, videoController.FindAll())
    })
}  
```
